//
//  JPResponderManagedObjectController.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import <UIKit/UIKit.h>
// add block headers

@class JPResponderManagedObjectConfiguration;

@interface JPResponderManagedObjectController : UITableViewController

@property (strong, nonatomic) JPResponderManagedObjectConfiguration *config;
@property (strong, nonatomic) NSManagedObject *messageObject;

- (NSManagedObject *)addRelationshipObjectForSection:(NSInteger)section;
- (void)removeRelationshipObjectInIndexPath:(NSIndexPath *)indexPath;

- (NSArray *)aggregateOperationOnEvent:(NSString *)attributeName withExpression:(NSString *)function withPredicate:(NSPredicate *)predicate inManagedObjectContext: (NSManagedObjectContext*)context withExpressionResultType:(NSAttributeType *)attributeType;
- (IBAction) showNotificationForResult:(BOOL)sent withDate:(NSDate *)sendDate withManagedObject:(NSManagedObject *)message;
@end