//
//  JPResponderDetailMessageEditCell.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderMessageCell.h"

@interface JPResponderDetailMessageEditCell : JPResponderMessageCell
// add protocols
<UITextFieldDelegate, UIAlertViewDelegate>

- (IBAction)validate;   // every textfield value

- (IBAction)pickerDoneClicked:(id)sender;   // dissmiss picker (inherited method to subclass cell)

@end