//
//  JPResponderMessageListViewController.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderMessageListViewController.h"
#import "JPAppDelegate.h"
#import "JPResponderManagedObjectController.h"

@interface JPResponderMessageListViewController ()
@property (strong, nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject *aMessage;
@property (strong, nonatomic) UISwitch *aSwitch;
@end

@implementation JPResponderMessageListViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Edit button in the navigation bar for this view controller if rows >= 1
    self.navigationItem.rightBarButtonItem = self.addMessageButton;
    
    //add switch and action
    self.aSwitch = [[UISwitch alloc]init];
    self.aSwitch.on = NO;
    
    // Select the Tab Bar button
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectedTab = [defaults integerForKey:kSelectedTabDefaultsKey];
    UITabBarItem *item = [self.officeTabBar.items objectAtIndex:selectedTab];
    [self.officeTabBar setSelectedItem:item];
    
    //set navigation title
    if (selectedTab == 0) self.navigationItem.title = @"Daily";
    else self.navigationItem.title = @"Weekly";
    
    // Fetch any existing entities
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error loading data", @"Error loading data")
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"Error was: %@, quitting.", @"Error was: %@, quitting."), [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.messageTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// change the right navigation item to add item (message count !=1) or edit item
- (void)showNavigationButton
{
    self.navigationItem.rightBarButtonItem = ([self.fetchedResultsController.fetchedObjects count] > 0) ?     self.navigationItem.rightBarButtonItem = self.editButtonItem : self.addMessageButton;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    //self.addMessageButton.enabled = !editing;
    [self.messageTable setEditing:editing animated:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"MessageDetailSegue"])
    {
        if ([sender isKindOfClass:[NSManagedObject class]])
        {
            JPResponderManagedObjectController *messageDetailController = segue.destinationViewController;
            messageDetailController.messageObject = sender;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Detail Message Error", @"Detail Message Error") message:NSLocalizedString(@"Error trying to show Message detail", @"Error trying to show Message detail") delegate:self cancelButtonTitle:NSLocalizedString(@"Aw", @"Aw") otherButtonTitles: nil];
            [alert show];
        }
    }
}

//turns on or off the selected message
- (IBAction)switchChanged:(UISwitch *)sender
{
    //all this just for a cell value...
    UITableViewCell *cell = (UITableViewCell *)sender.superview;
    UITableView *tableView = (UITableView *)cell.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    NSManagedObject * temp = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    //NSLog(@"the row is: %d", indexPath.row);
    //NSLog(@"the tag value is: %d", [self.aSwitch isOn]);
    //    NSLog(@"on %@", [NSNumber numberWithBool:YES]);
    
    if ([sender isOn])
        [temp setValue:[NSNumber numberWithBool:YES] forKey:@"isMessageOn"];
    
    else [temp setValue:0 forKey:@"isMessageOn"];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[[_fetchedResultsController sections] objectAtIndex:section] name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"TextMessageListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //check if cell is empty, if so, create a new table view cell using identifier
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    //add a switch for message on/off
    self.aSwitch = [[UISwitch alloc] init];
    [self.aSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    
    NSManagedObject *aMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSInteger tab = [self.officeTabBar.items indexOfObject:self.officeTabBar.selectedItem];
    
    // refactor this!
    switch (tab)     // show the right tabbar index view with messages
    {
        case kDailyTab:
            cell.textLabel.text = [aMessage valueForKey:@"textMessage"];
            cell.detailTextLabel.text = [aMessage valueForKey:@"messageRepeatingSequence"];
            break;
        case kWeeklyTab:
            cell.textLabel.text = [aMessage valueForKey:@"textMessage"];
            cell.detailTextLabel.text = [aMessage valueForKey:@"messageRepeatingSequence"];
            break;
        default:
            break;
    }
    
    //add the switch
    //add the switch with isMessageOn value
    cell.accessoryView = self.aSwitch;
    self.aSwitch.tag = indexPath.row;
    BOOL on = (BOOL)[aMessage valueForKey:@"isMessageOn"];
    if (on) self.aSwitch.on = YES;
    else self.aSwitch.on = NO;
    
    return cell;    // return cell
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObjectContext *managedObjectContext=[self.fetchedResultsController managedObjectContext];
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Delete the row from the data source
        [managedObjectContext deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error saving after delete", @"Error saving after delete.")
                                                          message:[NSString stringWithFormat:NSLocalizedString(@"Error was: %@, quitting.", @"Error was: %@,quitting."), [error localizedDescription]]
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                                otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *selectedMessage = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"MessageDetailSegue" sender:selectedMessage];
}

#pragma mark - UITabBarDelegate Methods

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSUInteger tabIndex = [tabBar.items indexOfObject:item];
    [defaults setInteger:tabIndex forKey:kSelectedTabDefaultsKey];
    
    //    NSLog(@"this is item in didselect %d ", tabIndex);
    
    if (tabIndex == 0) self.navigationItem.title = @"Daily";
    else self.navigationItem.title = @"Weekly";
    
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Error performing fetch: %@", [error localizedDescription]);
    }
    
    [self.messageTable reloadData];
}

#pragma mark - FetchedResultsController Property

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    JPAppDelegate *appDelegate = (JPAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ResponderTextMessage"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    NSUInteger tabIndex = [self.officeTabBar.items indexOfObject:self.officeTabBar.selectedItem];
    if (tabIndex == NSNotFound) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        tabIndex = [defaults integerForKey:kSelectedTabDefaultsKey];
    }
    
    NSString *sectionKey = @"groupMessageNumber";
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"textMessage" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"messageRepeatingSequence" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1,
                                sortDescriptor2, nil];
    switch (tabIndex)
    {
        case kDailyTab:
        {
            //sort by message repeating category daily
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.messageRepeatingSequence == %@", kMessageRepeatingDaily];
            [fetchRequest setPredicate:predicate];
            
            [fetchRequest setSortDescriptors:sortDescriptors];
            
            break;
        }
        case kWeeklyTab:{
            //sort by message repeating category weekly
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.messageRepeatingSequence == %@", kMessageRepeatingWeekly];
            [fetchRequest setPredicate:predicate];
            
            [fetchRequest setSortDescriptors:sortDescriptors];
            break;
        }
    }
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:sectionKey cacheName:nil];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.messageTable beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.messageTable endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id < NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.messageTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.messageTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.messageTable insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.messageTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
    }
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    exit(-1);
}

- (IBAction)addTextMessage:(id)sender
{
    NSManagedObjectContext *managedObjectContext = [self.fetchedResultsController managedObjectContext];
    
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    
    NSManagedObject *newMessage = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:managedObjectContext];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error saving entity", @"Error saving entity")
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"Error was: %@, quitting.", @"Error was: %@, quitting."), [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                              otherButtonTitles:nil];
        [alert show];
    }
    [self performSegueWithIdentifier:@"MessageDetailSegue" sender:newMessage];
}

@end