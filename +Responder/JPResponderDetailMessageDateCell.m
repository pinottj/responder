//
//  JPResponderDetailMessageDateCell.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderDetailMessageDateCell.h"

static NSDateFormatter *_dateFormatter = nil;
static NSDateFormatter *_timeFormatter = nil;

@interface JPResponderDetailMessageDateCell()
@property (strong, nonatomic) UIDatePicker *datePicker; // date picker
- (IBAction)datePickerChanged:(id)sender; // call back for change in date
@end

@implementation JPResponderDetailMessageDateCell
+ (void)initialize //class method for _dateFormatter
{
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [_timeFormatter setDateFormat:@"HH:mm:ss"];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.textField.clearButtonMode = UITextFieldViewModeNever;
        
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero]; // instantiate picker
        self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        [self.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
        
        self.textField.inputView = _datePicker;
        
        // adding dismiss button to picker
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        keyboardDoneButtonView.barStyle = UIBarStyleBlack;
        keyboardDoneButtonView.translucent = YES;
        keyboardDoneButtonView.tintColor = nil;
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(pickerDoneClicked:)];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        self.textField.inputAccessoryView = keyboardDoneButtonView;
    }
    return self;
}

// override inherited methods
#pragma mark - RDetailMessageEditCell Overrides

- (id)value
{
    if (self.textField.text == nil || [self.textField.text length] == 0)
        return nil;
    return self.datePicker.date; // picker has date, return it
}

- (void)setValue:(id)value
{
    if (value != nil && [value isKindOfClass:[NSDate class]])
    {
        //check initial set date for picker
        if ([self.datePicker.date compare:value] == NSOrderedDescending) {
            //date1 is earlier than date2
            //add 5 minutes to time
            self.datePicker.date = [self.datePicker.date dateByAddingTimeInterval:300];
            self.textField.text =[_dateFormatter stringFromDate:self.datePicker.date];
        }
        else{ //date2 is earlier than date1
            [self.datePicker setDate:value];
            self.textField.text =[_dateFormatter stringFromDate:value];
        }
    }
    else
        self.textField.text = nil;
}

#pragma mark - (Private) Instance Method

- (IBAction)datePickerChanged:(id)sender
{
    NSDate *date = [self.datePicker date];
    self.value = date;
    self.textField.text = [_dateFormatter stringFromDate:date];
}

#pragma mark - RDetailMessageEditCell Override

@end