//
//  JPResponderDetailMessagePickerCell.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderDetailMessageEditCell.h"

@interface JPResponderDetailMessagePickerCell : JPResponderDetailMessageEditCell
<UIPickerViewDataSource, UIPickerViewDelegate>  // add protocols

@property (strong, nonatomic) NSArray *values;  // property for selection list

@end
