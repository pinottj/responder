//
//  JPTextMessageReceiver.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//
#import "JPTextMessageReceiver.h"
#import "JPTextMessageReceiver.h"

@implementation JPTextMessageReceiver

@dynamic destinationCarrier;
@dynamic phoneNumber;
@dynamic receiverName;
@dynamic messages;

@end
