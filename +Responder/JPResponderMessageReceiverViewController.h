//
//  JPRMessageReceiverViewController.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderManagedObjectController.h"

@interface JPResponderMessageReceiverViewController : JPResponderManagedObjectController
<UISearchDisplayDelegate>
@property (copy, nonatomic) NSArray *allPeople;

@end
