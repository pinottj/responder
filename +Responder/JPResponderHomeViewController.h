//
//  JPResponderHomeViewController.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPResponderHomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *officeButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsBarButton;

@end
