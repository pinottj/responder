//
//  JPResponderTextMessage.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define kMessageValidationDomain @"com.josepinott.Responder.MessageValidationDomain"
#define kMessageValidationSenddateCode 1000
#define kMessageValidationMessage 1001

@class TextMessageReceiver;

@interface JPResponderTextMessage : NSManagedObject
@property (nonatomic, retain) NSString * groupMessageNumber;
@property (nonatomic, retain) NSNumber * isMessageOn;
@property (nonatomic, retain) NSString * messageRepeatingSequence;
@property (nonatomic, retain) NSDate * sendDate;
@property (nonatomic, retain) NSDate * sendTime;
@property (nonatomic, retain) NSString * textMessage;
@property (nonatomic, retain, readonly) NSNumber * ageOfMessage;
@property (nonatomic, retain) NSDate * startDay;
@property (nonatomic, retain) NSNumber * messagesScheduled;
@property (nonatomic, retain) NSSet *messageReceivers;
@property (nonatomic, retain) NSSet *subMessages;

@property (nonatomic, readonly) NSArray *olderMessages;
@property (nonatomic, readonly) NSArray *sameGroup;
@property (nonatomic, readonly) NSArray *messageOn;
@property (nonatomic, readonly) NSArray *dailyRepeatingSequence;


@end

@interface JPResponderTextMessage(ReceiverAccessors)
- (void)addTextMessageReceiverObject:(TextMessageReceiver *)value;
- (void)removeTextMessageReceiverObject:(TextMessageReceiver *)value;
- (void)addTextMessageReceiver:(NSSet *)value;
- (void)removeTextMessageReceiver:(NSSet *)value;
@end