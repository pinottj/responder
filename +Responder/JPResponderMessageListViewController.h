//
//  JPResponderMessageListViewController.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JPResponderHomeViewController.h"

#define kSelectedTabDefaultsKey @"Selected Tab"
static NSString * const kMessageRepeatingDaily = @"Every Day";
static NSString * const kMessageRepeatingWeekly = @"Every Week";

enum {
    kDailyTab,
    kWeeklyTab,
};

@interface JPResponderMessageListViewController : UIViewController
//add protocols
<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,  NSFetchedResultsControllerDelegate, UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITabBar *officeTabBar;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addMessageButton;
@property (weak, nonatomic) IBOutlet UITableView *messageTable;

- (IBAction)addTextMessage:(id)sender;

@end