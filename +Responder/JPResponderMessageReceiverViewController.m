//
//  JPRMessageReceiverViewController.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderMessageReceiverViewController.h"
#import "JPResponderManagedObjectConfiguration.h"
#import <AddressBook/AddressBook.h>

static NSString *SectionsTableIdentifier = @"CellIdentifier";

NSString *const kDenied = @"Access to address book is denied";
NSString *const kRestricted = @"Access to address book is restricted";

ABAddressBookRef addressBook;

@interface JPResponderMessageReceiverViewController ()

@end

@implementation JPResponderMessageReceiverViewController
{
    NSMutableArray *filteredNames;
    UISearchDisplayController *searchController;
}

- (void) displayMessage:(NSString *)paramMessage{
    [[[UIAlertView alloc] initWithTitle:nil
                                message:paramMessage
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (BOOL) doesPersonExistWithFirstName:(NSString *)paramFirstName
                             lastName:(NSString *)paramLastName
                        inAddressBook:(ABRecordRef)paramAddressBook{
    
    BOOL result = NO;
    
    if (paramAddressBook == NULL){
        NSLog(@"The address book is null.");
        return NO;
    }
    
    //NSArray *allPeople = (__bridge_transfer NSArray *)
    ABAddressBookCopyArrayOfAllPeople(paramAddressBook);
    
    NSUInteger peopleCounter = 0;
    for (peopleCounter = 0;
         peopleCounter < [self.allPeople count];
         peopleCounter++){
        
        ABRecordRef person = (__bridge ABRecordRef)
        [self.allPeople objectAtIndex:peopleCounter];
        
        NSString *firstName = (__bridge_transfer NSString *)
        ABRecordCopyValue(person, kABPersonFirstNameProperty);
        
        NSString *lastName = (__bridge_transfer NSString *)
        ABRecordCopyValue(person, kABPersonLastNameProperty);
        
        BOOL firstNameIsEqual = NO;
        BOOL lastNameIsEqual = NO;
        
        if ([firstName length] == 0 &&
            [paramFirstName length] == 0){
            firstNameIsEqual = YES;
        }
        else if ([firstName isEqualToString:paramFirstName]){
            firstNameIsEqual = YES;
        }
        
        if ([lastName length] == 0 &&
            [paramLastName length] == 0){
            lastNameIsEqual = YES;
        }
        else if ([lastName isEqualToString:paramLastName]){
            lastNameIsEqual = YES;
        }
        
        if (firstNameIsEqual &&
            lastNameIsEqual){
            return YES;
        }
        
    }
    
    return result;
    
}

- (BOOL) doesGroupExistWithGroupName:(NSString *)paramGroupName
                       inAddressBook:(ABAddressBookRef)paramAddressBook{
    
    BOOL result = NO;
    
    if (paramAddressBook == NULL){
        NSLog(@"The address book is null.");
        return NO;
    }
    
    NSArray *allGroups = (__bridge_transfer NSArray *)
    ABAddressBookCopyArrayOfAllGroups(paramAddressBook);
    
    NSUInteger groupCounter = 0;
    for (groupCounter = 0;
         groupCounter < [allGroups count];
         groupCounter++){
        
        ABRecordRef group = (__bridge ABRecordRef)
        [allGroups objectAtIndex:groupCounter];
        
        NSString *groupName = (__bridge_transfer NSString *)
        ABRecordCopyValue(group, kABGroupNameProperty);
        
        if ([groupName length] == 0 &&
            [paramGroupName length] == 0){
            return YES;
        }
        
        else if ([groupName isEqualToString:paramGroupName]){
            return YES;
        }
        
    }
    
    return result;
    
}

- (void) readFromAddressBook:(ABAddressBookRef)paramAddressBook
{
    self.allPeople = (__bridge NSMutableArray *)ABAddressBookCopyArrayOfAllPeople( paramAddressBook );
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UITableView *tableView = [[UITableView alloc]init];
    [tableView registerClass:[UITableViewCell class]
      forCellReuseIdentifier:SectionsTableIdentifier];
    
    filteredNames = [NSMutableArray array];
    UISearchBar *searchBar = [[UISearchBar alloc]
                              initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.tableView.tableHeaderView = searchBar;
    self.tableView.tableHeaderView.hidden = YES;
    
    searchController = [[UISearchDisplayController alloc]
                        initWithSearchBar:searchBar
                        contentsController:self];
    searchController.delegate = self;
    searchController.searchResultsDataSource = self;
    
    self.config = [[JPResponderManagedObjectConfiguration alloc] initWithResource:@"RMessageReceiverViewConfiguration"];
    [self requestPermissionForAddressBook];
    //[self.allPeople sortedArrayUsingSelector:@selector(compare:)];
    NSUInteger peopleCounter = 0;
    for (peopleCounter = 0;
         peopleCounter < [    self.allPeople count];
         peopleCounter++){
        
        ABRecordRef thisPerson =  (__bridge ABRecordRef)
        [    self.allPeople objectAtIndex:peopleCounter];
        
        NSString *firstName = (__bridge_transfer NSString *)
        ABRecordCopyValue(thisPerson, kABPersonFirstNameProperty);
        
        NSString *lastName = (__bridge_transfer NSString *)
        ABRecordCopyValue(thisPerson, kABPersonLastNameProperty);
        
        NSLog(@"First Name = %@", firstName);
        NSLog(@"Last Name = %@", lastName);
    }
}

- (void)requestPermissionForAddressBook
{
    CFErrorRef error = NULL;
    
    switch (ABAddressBookGetAuthorizationStatus()){
        case kABAuthorizationStatusAuthorized:{
            addressBook = ABAddressBookCreateWithOptions(NULL, &error);
            [self readFromAddressBook:addressBook];
            if (addressBook != NULL){
                CFRelease(addressBook);
            }
            break;
        }
        case kABAuthorizationStatusDenied:{
            [self displayMessage:kDenied];
            break;
        }
        case kABAuthorizationStatusNotDetermined:{
            addressBook = ABAddressBookCreateWithOptions(NULL, &error);
            ABAddressBookRequestAccessWithCompletion
            (addressBook, ^(bool granted, CFErrorRef error) {
                if (granted){
                    [self readFromAddressBook:addressBook];
                } else {
                    NSLog(@"Access was not granted");
                }
                if (addressBook != NULL){
                    CFRelease(addressBook);
                }
            });
            break;
        }
        case kABAuthorizationStatusRestricted:{
            [self displayMessage:kRestricted];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Override Table editing methods

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    if (editing == FALSE) self.tableView.tableHeaderView.hidden = YES;
    else
    {
        [self.tableView setEditing:editing animated:animated];
        self.tableView.tableHeaderView.hidden = NO;
    }
}

@end