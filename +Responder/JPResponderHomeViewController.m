//
//  JPResponderHomeViewController.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderHomeViewController.h"
#import "JPResponderSettingsViewController.h"
#import "JPResponderManagedObjectConfiguration.h"

@implementation JPResponderHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"wall.png"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    // set cogwheel for settings rightBarButton
    self.settingsBarButton.title = @"\u2699";
    UIFont *f1 = [UIFont fontWithName:@"Helvetica" size:24.0];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:f1, NSFontAttributeName, nil];
    [self.settingsBarButton setTitleTextAttributes:dict forState:UIControlStateNormal];
    // assign cogwheel to rightBarButtonItem
    self.navigationItem.rightBarButtonItem = self.settingsBarButton;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SettingsViewSegue"]) {
        JPResponderSettingsViewController *settingsController = segue.destinationViewController;
        settingsController.messageObject = sender;
    }
}

@end