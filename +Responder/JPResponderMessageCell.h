//
//  JPResponderMessageCell.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPResponderMessageCell : UITableViewCell

@property (strong, nonatomic) NSManagedObject *message;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) id value;

- (BOOL)isEditable;

@end