//
//  JPTextMessageReceiver.h
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JPResponderTextMessage;

@interface JPTextMessageReceiver : NSManagedObject

@property (nonatomic, retain) NSString * destinationCarrier;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) NSString * receiverName;
@property (nonatomic, retain) JPResponderTextMessage *messages;

@end
