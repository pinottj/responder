//
//  JPResponderDetailMessagePickerCell.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderDetailMessagePickerCell.h"

@interface JPResponderDetailMessagePickerCell()
@property (strong, nonatomic) UIPickerView *pickerView;
@end

@implementation JPResponderDetailMessagePickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.textField.clearButtonMode = UITextFieldViewModeNever;
        
        self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
        self.pickerView.dataSource = self;
        self.pickerView.delegate = self;
        self.pickerView.showsSelectionIndicator = YES;
        self.textField.inputView = self.pickerView;
        
        // adding dismiss button to picker
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        keyboardDoneButtonView.barStyle = UIBarStyleBlack;
        keyboardDoneButtonView.translucent = YES;
        keyboardDoneButtonView.tintColor = nil;
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(pickerDoneClicked:)];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        self.textField.inputAccessoryView = keyboardDoneButtonView;
    }
    return self;
}

#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.values count];
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.values objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.value = [self.values objectAtIndex:row];
}

#pragma mark - RDetailMessageEditCell Override

- (void)setValue:(id)value
{
    if (value != nil)
    {
        NSInteger index = [self.values indexOfObject:value];
        if (index != NSNotFound) self.textField.text = value; // set text field value
    }
    else self.textField.text = nil; // value not found
}

@end