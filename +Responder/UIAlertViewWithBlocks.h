/*
 Document:       UIAlertViewWithBlocks.h
 Program Name:   Responder
 Class:          CS 3903
 Professor:      Lartigue
 Created by:     Jose Pinott on 3/30/13
 Copyright (c) 2013 Jose Pinott. All rights reserved.
 */

#import <Foundation/Foundation.h>

typedef void (^DismissBlock)(int buttonIndex);
typedef void (^CancelBlock)();

@interface UIAlertView (Blocks) <UIAlertViewDelegate>

+ (UIAlertView*) showAlertViewWithTitle:(NSString*) title
                                message:(NSString*) message
                      cancelButtonTitle:(NSString*) cancelButtonTitle
                      otherButtonTitles:(NSArray*) otherButtons
                              onDismiss:(DismissBlock) dismissed
                               onCancel:(CancelBlock) cancelled;

@end
