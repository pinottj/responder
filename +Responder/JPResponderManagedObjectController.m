//
//  JPResponderManagedObjectControllerViewController.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderManagedObjectController.h"
#import "JPResponderManagedObjectConfiguration.h"
#import "JPResponderDetailMessageEditCell.h"
#import "JPResponderDetailMessageDateCell.h"
#import "JPResponderDetailMessagePickerCell.h"

NSString* const kBaseURL = @"http://www.students4students.com/send_sms.php";
NSString* const kDefaultPhone = @"4046715532";

@interface JPResponderManagedObjectController ()
@property (strong, nonatomic) UIBarButtonItem *saveButton;
@property (strong, nonatomic) UIBarButtonItem *backButton;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
- (void)save;
- (void)cancel;
- (void)updateDynamicSections:(BOOL)editing;
- (void)saveManagedObjectContext;
@end

@implementation JPResponderManagedObjectController
{
    NSString *textMessageRecieverCarrier;
    NSDate *sendTimeConfirmation;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

// override to switch the editButton to save
- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [self.tableView beginUpdates];
    [self updateDynamicSections:editing];
    [super setEditing:editing animated:animated];
    [self.tableView endUpdates];
    
    self.navigationItem.rightBarButtonItem = (editing) ? self.saveButton : self.editButtonItem;
    self.navigationItem.leftBarButtonItem = (editing) ? self.cancelButton : self.backButton;
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    // Instance of Save button
    self.saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
    
    self.backButton = self.navigationItem.leftBarButtonItem;
    self.cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    
    self.navigationItem.title = @"Message Info";    // Title for navigation controller
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    if ([self.tableView respondsToSelector:@selector(setSectionIndexColor:)]) {
        self.tableView.sectionIndexColor = [UIColor whiteColor];
        self.tableView.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.config numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = [self.config numberOfRowsInSection:section];
    if ([self.config isDynamicSection:section]) {
        NSString *key = [self.config dynamicAttributeKeyForSection:section];
        NSSet *attributeSet = [self.messageObject mutableSetValueForKey:key];
        rowCount = (self.editing) ? attributeSet.count+1 : attributeSet.count;
    }
    return rowCount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.config headerInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellClassname = [self.config cellClassnameForIndexPath:indexPath];
    
    JPResponderDetailMessageEditCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClassname];
    if (cell == nil)
    {
        Class cellClass = NSClassFromString(cellClassname);
        cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleValue2
                                reuseIdentifier:cellClassname];
    }
    // Configure the cell...
    cell.message = self.messageObject;
    
    NSArray *values = [self.config valuesForIndexPath:indexPath];
    
    if (values != nil) {
        // clean this up 
        [cell performSelector:@selector(setValues:) withObject:values];
    }
    cell.key = [self.config attributeKeyForIndexPath:indexPath];
    
    if ([self.config isDynamicSection:[indexPath section]])
    {
        NSString *key = [self.config attributeKeyForIndexPath:indexPath];
        NSMutableSet *relationshipSet = [self.messageObject mutableSetValueForKey:key];
        NSArray * relationshipArray = [relationshipSet allObjects];
        if ([indexPath row] != [relationshipArray count]) {
            NSManagedObject *relationshipObj = [relationshipArray objectAtIndex:[indexPath row]];
            cell.value = [relationshipObj valueForKey:@"receiverName"];
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
            cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
        else{
            cell.label.text = nil;
            cell.textField.text = @"Add New Contact";
        }
    }
    else {
        NSString *value = [[self.config rowForIndexPath:indexPath] objectForKey:@"value"];
        if (value != nil) {
            cell.value = value;
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
            cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
        else //here after cell id sets the value
            cell.value = [self.messageObject valueForKey:[self.config attributeKeyForIndexPath:indexPath]];
    }
    
    cell.label.text = [self.config labelForIndexPath:indexPath];
    
    return cell;    // return cell
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        [tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - Table view delegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellEditingStyle editStyle = UITableViewCellEditingStyleNone;
    NSInteger section = [indexPath section];
    
    if ([self.config isDynamicSection:section])
    {
        NSInteger rowCount = [self tableView:self.tableView numberOfRowsInSection:section];
        if ([indexPath row] == rowCount -1)
            editStyle = UITableViewCellEditingStyleInsert;
        else
            editStyle = UITableViewCellEditingStyleDelete;
    }
    return editStyle;
}

#pragma mark - (Private) Instance Methods

- (void)save
{
    [self setEditing:NO animated:YES];
    id value; // checks if sendDate is nill
    for (JPResponderDetailMessageEditCell *cell in [self.tableView visibleCells])
    {
        if ([cell isEditable])
        {
            // dress this up latter, switch or call seperate methods
            if ([[cell key] isEqual:@"sendDate"]) {
                value =[cell value];
            }
            if ([[cell key] isEqual: @"phoneNumber"])
            {
                NSString * num = [cell value];
                NSString * strippedNumber = [num stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [num length])];
                [self.messageObject setValue:strippedNumber forKey:[cell key]];
            }
            [self.messageObject setValue:[cell value] forKey:[cell key]];
        }
    }
    
    [self saveManagedObjectContext];
    [self.tableView reloadData];
    
    // check the entity for textmessage receiver
    if (![[self.messageObject entity].name isEqualToString:@"TextMessageReceiver"])
    {
        if (value){
            if ((BOOL)[self.messageObject valueForKey:@"messagesScheduled"]!= YES)
            {
                // set up queue with trigger time (rough)
                /* need to move this and change it to handle group dispatching
                 dispatch_time_t popTime = getDispatchTimeByDate([self.messageObject valueForKey:@"sendDate"]);
                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [self sendUrlRequestWithMessageObject:self.messageObject];});
                 [self.messageObject setValue:[NSNumber numberWithBool:YES]  forKey:@"messagesScheduled"];
                 */
                
                
               
                //send message asunchronously with GCD
                dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                
                dispatch_async(concurrentQueue, ^{
                    //send date and time, receiver and message
                    NSDate *time = [self.messageObject valueForKey:@"sendDate"];
                    
                    NSLog(@"this is the time, %@", time);
                    
                    dispatch_sync(concurrentQueue, ^{
                        //send the message here
                        [self sendUrlRequestWithMessageObject:self.messageObject];
                    });
                    
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        //get confirmation here
                        [self.messageObject setValue:[NSNumber numberWithBool:YES]  forKey:@"messagesScheduled"];

                        //UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message sent" message:@"Back In Main" delegate:self cancelButtonTitle:@"Quit" otherButtonTitles:nil];
                        //[alertView show];
                    });
                });
            }
        }
    }
}

- (void)cancel
{
    [self setEditing:NO animated:YES];
    [self.tableView reloadData];
    return;
}

- (void)updateDynamicSections:(BOOL)editing
{
    for (NSInteger section = 0; section < [self.config numberOfSections]; section++) {
        if ([self.config isDynamicSection:section]) {
            NSIndexPath *indexPath;
            NSInteger row = [self tableView:self.tableView numberOfRowsInSection:section];
            if (editing) {
                indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            else {
                indexPath = [NSIndexPath indexPathForRow:row-1 inSection:section];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
    }
}

- (void)saveManagedObjectContext
{
    NSError *error;
    if (![self.messageObject.managedObjectContext save:&error]) {
        [self setEditing:YES animated:YES];
    }
}

#pragma mark - Instance Methods

- (NSManagedObject *)addRelationshipObjectForSection:(NSInteger)section
{
    NSString *key = [self.config dynamicAttributeKeyForSection:section];
    NSMutableSet *relationshipSet = [self.messageObject mutableSetValueForKey:key];
    
    NSEntityDescription *entity = [self.messageObject entity];
    NSDictionary *relationships = [entity relationshipsByName];
    NSRelationshipDescription *destRelationship = [relationships objectForKey:key];
    NSEntityDescription *destEntity = [destRelationship destinationEntity];
    
    NSManagedObject *relationshipObject = [NSEntityDescription insertNewObjectForEntityForName:[destEntity name] inManagedObjectContext:self.messageObject.managedObjectContext];
    [relationshipSet addObject:relationshipObject];
    [self saveManagedObjectContext];
    return relationshipObject;
}

- (void)removeRelationshipObjectInIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [self.config dynamicAttributeKeyForSection:[indexPath section]];
    NSMutableSet *relationshipSet = [self.messageObject mutableSetValueForKey:key];
    NSManagedObject *relationshipObject = [[relationshipSet allObjects] objectAtIndex:[indexPath row]];
    [relationshipSet removeObject:relationshipObject];
    [self saveManagedObjectContext];
}

- (NSArray *)aggregateOperationOnEvent:(NSString *)attributeName withExpression:(NSString *)function withPredicate:(NSPredicate *)predicate inManagedObjectContext: (NSManagedObjectContext*)context withExpressionResultType:(NSAttributeType *)attributeType
{
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"ResponderTextMessage" inManagedObjectContext:context];
    
    NSAttributeDescription* attributeDesc = [entity.attributesByName objectForKey:attributeName];
    
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: attributeName];
    NSExpression *functionExpression = [NSExpression expressionForFunction: [NSString stringWithFormat:@"%@:", function] arguments: [NSArray arrayWithObject:keyPathExpression]];
    
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName: function];
    [expressionDescription setExpression: functionExpression];
    [expressionDescription setExpressionResultType: *attributeType];
    
    
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"ResponderTextMessage"];
    
    [request setPropertiesToFetch:[NSArray arrayWithObjects:attributeDesc, expressionDescription, nil]];
    [request setPropertiesToGroupBy:[NSArray arrayWithObject:attributeDesc]];
    [request setResultType:NSDictionaryResultType];
    
    if (predicate != nil)
        [request setPredicate:predicate];
    
    NSError* error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    return results;
}

- (IBAction) showNotificationForResult:(BOOL)sent withDate:(NSDate *)sendDate withManagedObject:(NSManagedObject *)message
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
	
	// Get the current date
	NSDate *pickerDate = sendDate;
	
	// Break the date up into components
	NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:pickerDate];
	NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:pickerDate];
	
	// Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
	[dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
	
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
	
	// Notification details
    if (sent) {
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"h:mm:ss a"]; //12hr time format
        NSString *dateString = [outputFormatter stringFromDate:[self.messageObject valueForKey:@"sendTime"]];
        localNotif.alertBody = [[NSString alloc]initWithFormat:@"Message Sent on %@", dateString];
    }
    else     localNotif.alertBody = @"Message Not Sent";
	
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
	
	// Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
    localNotif.userInfo = infoDict;
	
	// Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

-(void)sendUrlRequestWithMessageObject:(NSManagedObject *)message
{
    
    //set up url
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kBaseURL]];
    
    //Set the request's content type to application/x-www-form-urlencoded
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSString *from = kDefaultPhone; // default from message number
    // send information from core data
    NSString *to = [[message valueForKey:@"whoHas"] valueForKey:@"phoneNumber"];
    NSString *msg = [message valueForKey:@"textMessage"];
    
    // get carrier with format
    [self stringByReplacingOccurrences:[[[self.messageObject valueForKey:@"whoHas"] valueForKeyPath:@"destinationCarrier" ]description]];
    
    //incase of connection interruption
    [urlRequest setTimeoutInterval:30.0f];
    // Designate the request a POST request and specify its body data
    [urlRequest setHTTPMethod:@"POST"];
    
    //body of message
    NSString *body = [NSString stringWithFormat:@"from=%@&to=%@&carrier=%@&message=%@", from, to, textMessageRecieverCarrier,  msg];
    
    //this one works too, check for used practice
    //[urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    [urlRequest setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:[body length]]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
         if ([data length] >0  && error == nil){
             NSString *html = [[NSString alloc] initWithData:data
                                                    encoding:NSUTF8StringEncoding];
             if ([html isEqualToString:@"YES"])
             {
                 sendTimeConfirmation = [NSDate date];
                 [self.messageObject setValue:sendTimeConfirmation forKey:@"sendTime"];
                 [self showNotificationForResult:YES withDate:sendTimeConfirmation withManagedObject:self.messageObject];
                 
             }
             else  NSLog(@"sent = %@", html);
         }
         else if ([data length] == 0 &&
                  error == nil){
             NSLog(@"Nothing was sent!");
         }
         else if (error != nil){
             NSLog(@"Error happened = %@", error);
         }
     }];
}

dispatch_time_t getDispatchTimeByDate(NSDate *date)
{
    NSTimeInterval interval;
    double second, subsecond;
    struct timespec time;
    dispatch_time_t milestone;
    
    interval = [date timeIntervalSince1970];
    subsecond = modf(interval, &second);
    time.tv_sec = second;
    time.tv_nsec = subsecond * NSEC_PER_SEC;
    milestone = dispatch_walltime(&time, 0);
    
    return milestone;
}

- (NSString*)stringByReplacingOccurrences:(NSString*)text
{
    // strip carrier for non alpha and whitespace
    text = [text stringByReplacingOccurrencesOfString:@"{("
                                           withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@")}"
                                           withString:@""];
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    text = [text stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    return [self carrierWithFormat:text];
}

- (NSString*)carrierWithFormat:(NSString*)carrier
{
    // need to format destination carrier
    if ([carrier isEqualToString:@"AT&T"]) {textMessageRecieverCarrier = @"att";}
    else if ([carrier isEqualToString:@"T-Mobile"]) {textMessageRecieverCarrier = @"tmobile";}
    else textMessageRecieverCarrier = carrier;
    return carrier;
}

@end