//
//  JPResponderMessageDetailViewController.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderMessageDetailViewController.h"
#import "JPResponderManagedObjectConfiguration.h"
#import "JPResponderMessageReceiverViewController.h"

@interface JPResponderMessageDetailViewController ()

@end

@implementation JPResponderMessageDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // plist for rows and sections
    self.config = [[JPResponderManagedObjectConfiguration alloc] initWithResource:@"RDetailMessageConfiguration"];
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"MessageReceiverViewSegue"]) {
        if ([sender isKindOfClass:[NSManagedObject class]]) {
            JPResponderManagedObjectController *detailController = segue.destinationViewController;
            detailController.messageObject = sender;
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message Receiver Error", @"Message Receiver  Error") message:NSLocalizedString(@"Error trying to Message Receiver detail", @"Error trying to show Message Receiver detail") delegate:self cancelButtonTitle:NSLocalizedString(@"Aw, Ouch", @"Aw, Ouch") otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
        [self removeRelationshipObjectInIndexPath:indexPath];
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        NSManagedObject *newObject = [self addRelationshipObjectForSection:[indexPath section]];
        [self performSegueWithIdentifier:@"MessageReceiverViewSegue" sender:newObject];
    }
    
    [super tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [self.config attributeKeyForIndexPath:indexPath];
    NSMutableSet *relationshipSet = [self.messageObject mutableSetValueForKey:key];
    NSManagedObject *relationshipObj = [[relationshipSet allObjects] objectAtIndex: [indexPath row]];
    
    [self performSegueWithIdentifier:@"MessageReceiverViewSegue" sender:relationshipObj];
    
}

@end