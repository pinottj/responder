//
//  JPResponderDetailMessageEditCell.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderDetailMessageEditCell.h"
#import "UIAlertViewWithBlocks.h"

static NSDictionary *__CoreDataErrors;

@implementation JPResponderDetailMessageEditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.textField.delegate = self;
        
        // added done as return type
        self.textField.returnKeyType = UIReturnKeyDone;
        [self.textField addTarget:self
                           action:@selector(textFieldDone:)
                 forControlEvents:UIControlEventEditingDidEndOnExit];
    }
    return self;
}

+ (void)initialize
{
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"CoreDataErrors" withExtension:@"plist"];
    __CoreDataErrors = [NSDictionary dictionaryWithContentsOfURL:plistURL];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    self.textField.enabled = editing;
}

- (void)textFieldDone:(id)sender
{
    [sender resignFirstResponder];
}


#pragma mark UITextFieldDelegate methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // add this in later for returns on empty string
    //if ([[self.message valueForKey:@"textMessage"] isEqualToString: @"Enter Message"])return;
    [self validate];
}

#pragma mark Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex])
        [self setValue:[self.message valueForKey:self.key]];
    
    else [self.textField becomeFirstResponder];
}

#pragma mark - ResponderMessageCell Overrides

- (BOOL)isEditable
{
    return YES;
}


#pragma mark - Instance Methods

- (IBAction)validate
{
    id val = self.value;
    NSError *error;
    if (![self.message validateValue:&val forKey:self.key error:&error])
    {
        NSString *message = nil;
        if ([[error domain] isEqualToString:@"NSCocoaErrorDomain"])
        {
            NSString *errorCodeStr = [NSString stringWithFormat:@"%d", [error code]];
            NSString *errorMessage = [__CoreDataErrors valueForKey:errorCodeStr];
            NSDictionary *userInfo = [error userInfo];
            message = [NSString stringWithFormat:NSLocalizedString(@"Validation error on: %@\rFailure Reason: %@", @"Validation error on: %@, Failure Reason: %@)"), [userInfo valueForKey:@"NSValidationErrorKey"], errorMessage];
        }
        else
            message = [error localizedDescription];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", @"Message Error") message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Fix", @"Fix"), nil];
        [alert show];
    }
}

- (IBAction)pickerDoneClicked:(id)sender
{
    [self.textField resignFirstResponder];
}

@end