//
//  JPResponderTextMessage.m
//  +Responder
//
//  Created by Joey on 8/24/13.
//  Copyright (c) 2013 JosePinott. All rights reserved.
//

#import "JPResponderTextMessage.h"

@implementation JPResponderTextMessage

@dynamic groupMessageNumber;
@dynamic isMessageOn;
@dynamic messageRepeatingSequence;
@dynamic sendDate;
@dynamic sendTime;
@dynamic textMessage;
@dynamic ageOfMessage;
@dynamic startDay;
@dynamic messageReceivers;
@dynamic subMessages;
@dynamic olderMessages, sameGroup, messageOn, dailyRepeatingSequence, messagesScheduled;

- (void)awakeFromInsert
{
    self.startDay = [NSDate date];
    self.sendDate = self.startDay;
    self.messagesScheduled = FALSE;
    [super awakeFromInsert];
}

- (BOOL)validateSendDate:(id *)ioValue error:(NSError **)outError
{
    NSDate *date = *ioValue;
    if ([date compare:[NSDate date]] == NSOrderedAscending)
    {
        if (outError != NULL) {
            NSString *errorStr = NSLocalizedString(@"Send date cannot be in the past", @"Send date cannot be in the past");
            NSDictionary *userInfoDict = [NSDictionary dictionaryWithObject:errorStr forKey:NSLocalizedDescriptionKey];
            NSError *error = [[NSError alloc] initWithDomain:kMessageValidationDomain code:kMessageValidationSenddateCode userInfo:userInfoDict];
            *outError = error;
        }
        if ([self.startDay isEqualToDate:[NSDate date]]) {
            return NO;  // on cancel return false
        }
    }
    return YES;
}

- (BOOL)validateTextMessage:(NSError **)outError
{
    if ((0 == [self.textMessage length]))
    {
        if (outError != NULL) {
            NSString *errorStr = NSLocalizedString(@"Must provide message.", @"Must provide message.");
            NSDictionary *userInfoDict = [NSDictionary dictionaryWithObject:errorStr forKey:NSLocalizedDescriptionKey];
            NSError *error = [[NSError alloc] initWithDomain:kMessageValidationDomain code:kMessageValidationMessage userInfo:userInfoDict];
            *outError = error;
            
            return FALSE;  // empty message
        }
        
    }
    return YES;
}

- (BOOL)validateForInsert:(NSError **)outError
{
    return [self validateTextMessage:outError];
}

- (BOOL)validateForUpdate:(NSError **)outError
{
    return [self validateTextMessage:outError];
}

// virtual accessor
- (NSNumber *)ageOfMessage
{
    if (self.startDay == nil)
        return nil;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSYearCalendarUnit fromDate:self.startDay toDate:[NSDate date]options:0];
    NSInteger days = [components day];
    
    //    NSLog(@"age is %@", self.ageOfMessage);
    
    return [NSNumber numberWithInteger:days];
}
/*
 - (void)setSendTime:(NSDate *)sendTime
 {
 self.sendTime = sendTime;
 return;
 }
 */
@end