/*
 Document:      JPResponderSettingsViewController.h
 Program Name:  +Responder
 Created by:    Jose Pinott on 8/18/13.
 Copyright (c) 2013 JosePinott. All rights reserved.
 */

#import "JPResponderManagedObjectController.h"

#define kPhoneNumberKey         @"defaultPhone"
#define kTextMessageSubject     @"defaultSubject"

@interface JPResponderSettingsViewController : JPResponderManagedObjectController

@property (weak, nonatomic) IBOutlet UILabel *defaultPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *defaultSubjectLabel;

@end

//@class JPResponderManagedObjectConfiguration;