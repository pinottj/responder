/*
 Document:      JPResponderSettingsViewController.m
 Program Name:  +Responder
 Created by:    Jose Pinott on 8/18/13.
 Copyright (c) 2013 JosePinott. All rights reserved.
 */

#import "JPResponderSettingsViewController.h"
#import "JPResponderManagedObjectConfiguration.h"
//#import "ResponderTextMessage.h"

@implementation JPResponderSettingsViewController
{
//    ResponderTextMessage *message;
    NSUserDefaults *defaults;
    NSString *initialText;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    defaults = [NSUserDefaults standardUserDefaults];
    self.defaultPhoneLabel.text = [defaults objectForKey:kPhoneNumberKey];
    self.defaultSubjectLabel.text = [defaults objectForKey:kTextMessageSubject];
    [defaults synchronize];
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    return @"Default Phone Settings";
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             @"CellIdentifier"];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                      reuseIdentifier:@"CellIdentifier"];
    // Configure the cell...
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Phone:";
        cell.detailTextLabel.text = [defaults objectForKey:kPhoneNumberKey];
        cell.detailTextLabel.tag = 0;
    }
    else
    {
        cell.textLabel.text = @"Subject:";
        cell.detailTextLabel.text = [defaults objectForKey:kTextMessageSubject];
        cell.detailTextLabel.tag = 1;
    }
    return cell;
}
@end